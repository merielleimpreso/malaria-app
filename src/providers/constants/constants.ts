import { Injectable } from '@angular/core';

declare var window;

@Injectable()
export class ConstantsProvider {
  SOS_CONTACT: any = encodeURIComponent("+33 1 55 63 33 48");
  INTL_SOS_COUNTRY: any = "https://www.internationalsos.com/members_home/login/login.cfm";
  WHO: any = "http://www.who.int/malaria/en/";
  CDC: any = "https://www.cdc.gov/parasites/malaria/index.html";
  MED_SUPPLY: any = "http://www.medsupply.com";
  INTL_SOS_WEBSITE: any = "http://www.internationalsos.com";
  currentPage: any = 'login';

  constructor() {
  }

  callSos() {
    window.location = "tel:" + this.SOS_CONTACT;
  }

  openLink(link) {
    window.location = link;
  }

  setCurrentPage(page) {
    this.currentPage = page;
  }
}
