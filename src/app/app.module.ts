import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { AbcdPage } from '../pages/abcd/abcd';
import { AcceptPage } from '../pages/accept/accept';
import { BSelectionPage } from '../pages/b-selection/b-selection';
import { HomePage } from '../pages/home/home';
import { InfoPage } from '../pages/info/info';
import { KitContentsPage } from '../pages/kit-contents/kit-contents';
import { KitFlapPage } from '../pages/kit-flap/kit-flap';
import { KitPage } from '../pages/kit/kit';
import { LoginPage } from '../pages/login/login';
import { QuizIntroPage } from '../pages/quiz-intro/quiz-intro';
import { QuizPage } from '../pages/quiz/quiz';
import { VideoPage } from '../pages/video/video';
import { ConstantsProvider } from '../providers/constants/constants';

@NgModule({
  declarations: [
    MyApp,
    AbcdPage,
    AcceptPage,
    BSelectionPage,
    HomePage,
    InfoPage,
    KitContentsPage,
    KitFlapPage,
    KitPage,
    LoginPage,
    QuizIntroPage,
    QuizPage,
    VideoPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {backButtonText: ''}),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AbcdPage,
    AcceptPage,
    BSelectionPage,
    HomePage,
    InfoPage,
    KitContentsPage,
    KitFlapPage,
    KitPage,
    LoginPage,
    QuizIntroPage,
    QuizPage,
    VideoPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ConstantsProvider
  ]
})
export class AppModule {}
