import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { ConstantsProvider } from '../providers/constants/constants';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { Storage } from '@ionic/storage';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = null;

  constructor(private constants: ConstantsProvider, platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private storage: Storage) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleLightContent();
      splashScreen.hide();

      storage.get('login').then((value) => {
        if (value == true) {
          this.constants.setCurrentPage('home');
          this.rootPage = HomePage;
          //this.rootPage = LoginPage;
        } else {
          this.constants.setCurrentPage('login');
          this.rootPage = LoginPage;
        }
      });
    });
  }
}
