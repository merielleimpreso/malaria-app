import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ConstantsProvider } from '../../providers/constants/constants';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-accept',
  templateUrl: 'accept.html',
})
export class AcceptPage {

  constructor(private constants: ConstantsProvider, public navCtrl: NavController, public navParams: NavParams) {
  }

  onClickAgree() {
    this.constants.setCurrentPage('home');
    this.navCtrl.setRoot(HomePage);
  }

}
