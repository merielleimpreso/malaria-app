import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { KitFlapPage } from '../kit-flap/kit-flap';

@Component({
  selector: 'page-kit-contents',
  templateUrl: 'kit-contents.html',
})
export class KitContentsPage {
  title: any;
  contents: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.contents = navParams.get('contents');
    if (this.contents == 'curative') {
      this.title = 'Malaria Curative Kit';
    } else if (this.contents == 'diagnostic') {
      this.title = 'Malaria Diagnostic Kit';
    }
  }

  onTapKit(kit) {
    this.navCtrl.push(KitFlapPage, {contents: this.contents, kit: kit})
  }

}
