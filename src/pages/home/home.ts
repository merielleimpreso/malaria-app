import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ConstantsProvider } from '../../providers/constants/constants';
import { AbcdPage } from '../abcd/abcd';
import { InfoPage } from '../info/info';
import { KitPage } from '../kit/kit';
import { QuizIntroPage } from '../quiz-intro/quiz-intro';
import { VideoPage } from '../video/video';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(private constants: ConstantsProvider, public navCtrl: NavController) {

  }

  ionViewWillEnter() {
    this.constants.setCurrentPage('home');
  }

  onTapSettings() {
    console.log("tapped settings");
  }

  onTapMyKitContents() {
    this.navCtrl.push(KitPage);
  }

  onTapHowToUse() {
    this.navCtrl.push(VideoPage, {title: "How To Use My Kit", video:"how.mp4", icon:"custom-how"});
  }

  onTapAbcd() {
    this.navCtrl.push(AbcdPage);
  }

  onTapMoreInfo() {
    this.navCtrl.push(InfoPage);
  }

  onTapQuiz() {
    this.navCtrl.push(QuizIntroPage, null, {animate: false});
  }
}
