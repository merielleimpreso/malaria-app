import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ConstantsProvider } from '../../providers/constants/constants';
import { KitContentsPage } from '../kit-contents/kit-contents';

@Component({
  selector: 'page-kit',
  templateUrl: 'kit.html',
})
export class KitPage {
  constructor(private constants: ConstantsProvider, public navCtrl: NavController, public navParams: NavParams) {
  }

  onTapCurativeKit() {
    this.navCtrl.push(KitContentsPage, {contents: 'curative'});
  }

  onTapDiagnosticKit() {
    this.navCtrl.push(KitContentsPage, {contents: 'diagnostic'});
  }
}
