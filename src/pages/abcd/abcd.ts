import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { BSelectionPage } from '../b-selection/b-selection';
import { VideoPage } from '../video/video';

@Component({
  selector: 'page-abcd',
  templateUrl: 'abcd.html',
})
export class AbcdPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }

  playVideo(title, video) {
    this.navCtrl.push(VideoPage, {title: title, video: video});
  }

  onClickB() {
    this.navCtrl.push(BSelectionPage);
  }

}
