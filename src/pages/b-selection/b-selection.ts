import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { VideoPage } from '../video/video';

@Component({
  selector: 'page-b-selection',
  templateUrl: 'b-selection.html',
})
export class BSelectionPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  playVideo(title, video) {
    this.navCtrl.push(VideoPage, {title: title, video: video});
  }

}
