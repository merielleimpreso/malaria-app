import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-kit-flap',
  templateUrl: 'kit-flap.html',
})
export class KitFlapPage {
  contents: any;
  kit: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.contents = navParams.get('contents');
    this.kit = navParams.get('kit');
  }

}
