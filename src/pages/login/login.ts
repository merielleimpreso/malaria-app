import { Component } from '@angular/core';
import { AlertController, NavController, NavParams } from 'ionic-angular';
import { AcceptPage } from '../accept/accept';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  correctCodes: any = [
    '00039',
    '00046',
    '00048',
    '00067',
    '00390',
    '00391',
    '00418',
    '00419',
    '00420',
    '00421',
    '00422',
    '00445',
    '00446'
  ];
  code: any;
  isLoaded: boolean = false;

  constructor(
    private alertCtrl:AlertController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage
  ) {
  }

  onClickSubmit() {
    for (let i = 0; i < this.correctCodes.length; i++) {
      if (this.correctCodes[i] == this.code) {
        this.navCtrl.setRoot(AcceptPage);
        this.storage.set('login', true);
        return;
      }
    }
    this.showAlert();
  }

  showAlert() {
    let alert = this.alertCtrl.create({
      title: 'Wrong Code!',
      subTitle: 'Please enter a different code.',
      buttons: ['Dismiss']
    });
    alert.present();
  }

}
