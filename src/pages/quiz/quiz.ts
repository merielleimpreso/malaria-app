import * as _ from 'lodash';
import { Component, ViewChild } from '@angular/core';
import { Content, NavController, NavParams, Slides } from 'ionic-angular';
import { AbcdPage } from '../abcd/abcd';
import { ConstantsProvider } from '../../providers/constants/constants';
import { QuizValues } from './quiz-values';
import { Storage } from '@ionic/storage';
import { normalizeURL } from 'ionic-angular';

@Component({
  selector: 'page-quiz',
  templateUrl: 'quiz.html',
})
export class QuizPage {
  @ViewChild(Slides) slides: Slides;
  @ViewChild(Content) content: Content;

  quiz: any = QuizValues;
  number: any = 0;
  canSubmit: any = false;
  show: any = 'question';
  isCorrect: any = false;
  points: any = 0;
  state: any = null;

  correctSfx: any;
  wrongSfx: any;

  constructor(
    private constants: ConstantsProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage) {
    this.state = navParams.get('state');
  }

  ionViewWillEnter() {
    this.constants.setCurrentPage('quiz');

    let correctSfxFile = normalizeURL('assets/audio/correct.mp3');
    let wrongSfxFile = normalizeURL('assets/audio/wrong.mp3');
    this.correctSfx = new Audio(correctSfxFile);
    this.wrongSfx = new Audio(wrongSfxFile);
  }

  ionViewWillLeave() {
    let state = {
      quiz: this.quiz,
      number: this.number,
      canSubmit: this.canSubmit,
      show: this.show,
      isCorrect: this.isCorrect,
      points: this.points
    };
    this.storage.set('state', state);
  }

  ionViewDidLoad() {
    if (this.state) {
      this.quiz = this.state.quiz;
      this.number = this.state.number;
      this.canSubmit = this.state.canSubmit;
      this.show = this.state.show;
      this.isCorrect = this.state.isCorrect;
      this.points = this.state.points;

      if (this.number > 0) {
        if (this.number < this.quiz.length) {
          setTimeout(() => {
            this.slides.slideTo(this.number, 500);
          }, 500);
        } else {
          this.initializeQuiz();
        }
      }
    } else {
      this.initializeQuiz();
    }
  }

  initializeQuiz() {
    this.number = 0;
    this.canSubmit = 0;
    this.show = 'question';
    this.isCorrect = 'false';
    this.quiz = QuizValues;
    this.points = 0;

    for (let i = 0; i < this.quiz.length; i++) {
      let q = this.quiz[i];
      for(let j = 0; j < q.choices.length; j++) {
        q.choices[j].isSelected = false;
      }
      this.quiz[i].choices = this.shuffleArray(q.choices);
      this.quiz[i].number = i+1;
      this.quiz[i].points = 0;
      this.quiz[i].isSelected = false;

      if (q.layout == 'square' || q.layout == 'rect') {
        let square = [];
        for (let i = 0; i < Math.ceil(q.choices.length / 2); i++) {
          square.push(i);
        }
        this.quiz[i].square = square;
      }
    }
  }

  onClickChoice(c) {
    c.isSelected = !c.isSelected;
    let item = this.quiz[this.number];
    for (let i = 0; i < item.choices.length; i++) {
      if (item.choices[i].choice == c.choice) {
        item.choices[i].isSelected = c.isSelected;
      } else {
        if (this.quiz[this.number].answer.length == 1) {
          if (c.isSelected) {
           item.choices[i].isSelected = false;
          }
        }
      }
    }
    this.checkCanSubmit();
  }

  onClickSubmit() {
    let answer = [];
    let item = this.quiz[this.number];
    for (let i = 0; i < item.choices.length; i++) {
      if (item.choices[i].isSelected == true) {
        answer.push(item.choices[i].choice);
      }
    }
    answer = answer.sort();
    this.isCorrect = _.isEqual(answer.sort(), item.answer.sort());
    if (this.isCorrect) {
      item.points = 30;
      this.quiz[this.number] = item;
    }
    this.show = 'result';
    this.content.scrollToTop();
    if (this.isCorrect) {
      this.correctSfx.play();
    } else {
      this.wrongSfx.play();
    }
  }

  onClickNext() {
    this.number = this.number+1;
    if (this.number < this.quiz.length) {
      this.canSubmit = false;
      this.show = 'question';
      this.isCorrect = false;
    } else {
      for (let i = 0; i < this.quiz.length; i++) {
        this.points += this.quiz[i].points;
      }
    }
    this.slides.slideNext();
    this.content.scrollToTop();
  }

  onClickWatch() {
    this.navCtrl.popToRoot({animate: false});
    this.navCtrl.push(AbcdPage);
  }

  onClickRepeatTheQuiz() {
    this.navCtrl.pop();
  }

  onClickBackToHome() {
    this.navCtrl.popToRoot();
  }

  shuffleArray(array,) {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
  }

  checkCanSubmit() {
    let item = this.quiz[this.number];
    for (let i = 0; i < item.choices.length; i++) {
      if (item.choices[i].isSelected == true) {
        this.canSubmit = true;
        return;
      }
    }
    this.canSubmit = false;
  }

}
