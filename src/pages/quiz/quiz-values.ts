export const QuizValues =
[
  {
    question: "What should you do to prepare for your trip?",
    question_image: null,
    choices: [
      {
        choice: 'a',
        value: 'See a doctor at least 4 to 6 weeks before departure',
        image: 'quiz-1-a.png'
      },
      {
        choice: 'b',
        value: 'Research whether malaria is a risk at your destination',
        image: 'quiz-1-b.png'
      },
      {
        choice: 'c',
        value: 'Purchase preventive malaria medication if prescribed for you',
        image: 'quiz-1-c.png'
      },
      {
        choice: 'd',
        value: 'Make sure routine and travel vaccinations are current',
        image: 'quiz-1-d.png'
      },
      {
        choice: 'e',
        value: 'Pack an effective insect repellent',
        image: 'quiz-1-e.png'
      }
    ],
    answer: ['a', 'b', 'c', 'd', 'e'],
    result: 'You should',
    result_list: [
      'see a doctor at least 4 to 6 weeks before departure',
      'research whether malaria is a risk at your destination',
      'purchase preventive malaria medication if prescribed for you',
      'make sure routine and travel vaccinations are current',
      'pack an effective insect repellent'
    ],
    result_image: 'quiz-1-result.gif',
    layout: 'list',
  },
  {
    question: "The risk of getting malaria is increased by",
    question_image: null,
    choices: [
      {
        choice: 'a',
        value: 'Sleeping under a bed net',
        image: 'quiz-2-a.png'
      },
      {
        choice: 'b',
        value: 'Staying outdoors between dusk and dawn',
        image: 'quiz-2-b.png'
      },
      {
        choice: 'c',
        value: 'Staying in rural areas',
        image: 'quiz-2-c.png'
      },
      {
        choice: 'd',
        value: 'Wearing sunscreen',
        image: 'quiz-2-d.png'
      }
    ],
    answer: ['b', 'c'],
    result: 'The risk of getting malaria is increased by',
    result_list: [
      'staying outdoor between dusk and dawn',
      'staying in rural areas'
    ],
    result_image: 'quiz-2-result.gif',
    layout: 'square',
  },
  {
    question: "Malaria is transmitted to people through",
    question_image: null,
    choices: [
      {
        choice: 'a',
        value: 'A bee sting',
        image: 'quiz-3-a.png'
      },
      {
        choice: 'b',
        value: 'A snake bite',
        image: 'quiz-3-b.png'
      },
      {
        choice: 'c',
        value: 'Mouse’s urine',
        image: 'quiz-3-c.png'
      },
      {
        choice: 'd',
        value: 'A mosquito bite',
        image: 'quiz-3-d.png'
      }
    ],
    answer: ['d'],
    result: 'Malaria is provoked by a',
    result_bold: 'mosquito bite.',
    result_list: [],
    result_image: 'quiz-3-result.gif',
    layout: 'square',
  },
  {
    question: "What is the causal agent of malaria?",
    question_image: 'quiz-4-question.gif',
    choices: [
      {
        choice: 'a',
        value: 'A bacteria',
        image: null
      },
      {
        choice: 'b',
        value: 'A parasite',
        image: null
      },
      {
        choice: 'c',
        value: 'A mineral',
        image: null
      },
      {
        choice: 'd',
        value: 'A virus',
        image: null
      },
      {
        choice: 'e',
        value: 'A chemical',
        image: null
      }
    ],
    answer: ['b'],
    result: 'Malaria is due to a',
    result_bold: 'parasite.',
    result_list: [],
    result_image: 'quiz-4-result.gif',
    layout: 'rect',
  },
  {
    question: "Where in the world is malaria present?",
    question_image: 'quiz-5-question.gif',
    choices: [
      {
        choice: 'a',
        value: 'Parts of South America',
        image: null
      },
      {
        choice: 'b',
        value: 'Parts of Africa',
        image: null
      },
      {
        choice: 'c',
        value: 'Parts of Asia',
        image: null
      },
      {
        choice: 'd',
        value: 'Antarctica',
        image: null
      },
      {
        choice: 'e',
        value: 'High altitude',
        image: null
      }
    ],
    answer: ['a', 'b', 'c'],
    result: 'Malaria is present in parts of South America, parts of Africa and parts of Asia.',
    result_list: [],
    result_image: 'quiz-5-result.gif',
    layout: 'rect',
  },
  {
    question: "During what period of the day are you at greatest risk of getting bitten by a malaria-transmitting mosquito?",
    question_image: 'quiz-6-question.gif',
    choices: [
      {
        choice: 'a',
        value: '10am',
        image: null
      },
      {
        choice: 'b',
        value: '3pm',
        image: null
      },
      {
        choice: 'c',
        value: 'Noon',
        image: null
      },
      {
        choice: 'd',
        value: 'From dusk to dawn',
        image: null
      }
    ],
    answer: ['d'],
    result: 'You are at greatest risk of getting bitten by a malaria-transmitting mosquito from dusk to dawn!',
    result_list: [],
    result_image: 'quiz-6-result.gif',
    layout: 'rect',
  },
  {
    question: "Malaria symptoms...",
    question_image: null,
    choices: [
      {
        choice: 'a',
        value: 'Can get rapidly worse',
        image: null
      },
      {
        choice: 'b',
        value: 'Start gradually, and take weeks to get worse',
        image: null
      },
      {
        choice: 'c',
        value: 'Can include fever, sweating and chills',
        image: null
      },
      {
        choice: 'd',
        value: 'Can include nausea, vomiting and diarrhea',
        image: null
      },
      {
        choice: 'e',
        value: 'Can be similar to the flu',
        image: null
      },
      {
        choice: 'f',
        value: 'Are obvious so there is no need to get a blood test',
        image: null
      },
    ],
    answer: ['a', 'c', 'd', 'e'],
    result: 'Malaria symptoms...',
    result_list: [
      "can get rapidly worse",
      "can include fever sweating and chills",
      "can include nausea, vomiting and diarrhea",
      "can be similar to the flu"
    ],
    result_image: 'quiz-7-result.gif',
    layout: 'list',
  },
  {
    question: "Select the true statements about preventing malaria.",
    question_image: null,
    choices: [
      {
        choice: 'a',
        value: 'If you have sprayed your room with insecticide before going to bed, you do not need to use a bed net',
        image: null
      },
      {
        choice: 'b',
        value: 'You need to prevent mosquito bites even if you are taking malaria prevention medication (chemoprophylaxis)',
        image: null
      },
      {
        choice: 'c',
        value: 'Sunscreen makes insect repellent ineffective',
        image: null
      },
      {
        choice: 'd',
        value: 'If prescribed malaria medication, you don’t need to start it until 7 days after arriving in the malarial area',
        image: null
      },
      {
        choice: 'e',
        value: 'If prescribed for you, it is safe to take anti-malarial medication for long periods of time',
        image: null
      }
    ],
    answer: ['b', 'e'],
    result: 'The true statements are:',
    result_list: [
      "you need to prevent mosquito bites even if you are taking malaria prevention medication (chemoprophylaxis).",
      "if prescribed for you, it is safe to take anti-malarial medication for long periods of time."
    ],
    result_image: 'quiz-8-result.gif',
    layout: 'list',
  },
  {
    question: "Which of the following measures help prevent mosquito bites?",
    question_image: null,
    choices: [
      {
        choice: 'a',
        value: 'Avoiding outdoor activities at night',
        image: null
      },
      {
        choice: 'b',
        value: 'Wearing clothing treated with insecticide',
        image: null
      },
      {
        choice: 'c',
        value: 'Wearing an anti-mosquito bracelet',
        image: null
      },
      {
        choice: 'd',
        value: 'Applying insect-repellent on uncovered parts of the body',
        image: null
      },
      {
        choice: 'e',
        value: 'Chewing garlic when going outdoors',
        image: null
      },
      {
        choice: 'f',
        value: 'Applying tea tree oil on uncovered parts of the body',
        image: null
      },
      {
        choice: 'g',
        value: 'Taking B1 vitamin tablets regularly',
        image: null
      },
      {
        choice: 'h',
        value: 'Wearing clothes that cover as much skin as is practical – such as long sleeve shirts, long pants and socks',
        image: null
      },
    ],
    answer: ['a', 'b', 'd', 'h'],
    result: 'The following measures help prevent mosquito bites:',
    result_list: [
      "avoiding outdoor activities at night",
      "wearing clothing treated with insecticide",
      "applying insect-repellent on uncovered parts of the body",
      "wearing clothes that covers as much skin as practical",
    ],
    result_image: 'quiz-9-result.gif',
    layout: 'list',
  },
  {
    question: "Which of the following are true?",
    question_image: null,
    choices: [
      {
        choice: 'a',
        value: 'Expats living in malaria-affected countries become immune to malaria, and remain immune for the rest of their lives',
        image: 'quiz-10-a.png'
      },
      {
        choice: 'b',
        value: 'See a doctor urgently if you develop a fever or other symptoms of malaria even after you have left the malarial area',
        image: 'quiz-10-b.png'
      },
      {
        choice: 'c',
        value: 'If you think you might have malaria, tell the doctor and request a blood test for malaria',
        image: 'quiz-10-c.png'
      },
      {
        choice: 'd',
        value: 'Malaria is an emergency',
        image: 'quiz-10-d.png'
      },
      {
        choice: 'e',
        value: 'Malaria can be fatal',
        image: 'quiz-10-e.png'
      }
    ],
    answer: ['b', 'c', 'd', 'e'],
    result: 'The true statements are:',
    result_list: [
      "see a doctor urgently if you develop a fever or other symptoms of malaria even after you have left the malaria area",
      "if you think you might have malaria tell the doctor and request a blood test for malaria",
      "malaria is an emergency",
      "malaria can be fatal",
    ],
    result_image: 'quiz-10-result.gif',
    layout: 'list',
  }
];
