import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { normalizeURL } from 'ionic-angular';

@Component({
  selector: 'page-video',
  templateUrl: 'video.html',
})
export class VideoPage {
  title: any;
  video: any;
  icon: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.title = navParams.get('title');
    this.video = normalizeURL('assets/videos/' + navParams.get('video') + '#t=0.1');
    console.log(this.video);
    this.icon = navParams.get('icon');
    if (!this.icon) {
      this.icon = "custom-abcd";
    }
  }

}
