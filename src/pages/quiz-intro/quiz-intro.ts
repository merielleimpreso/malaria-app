import { Component } from '@angular/core';
import { AlertController, NavController, NavParams } from 'ionic-angular';
import { ConstantsProvider } from '../../providers/constants/constants';
import { QuizPage } from '../quiz/quiz';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-quiz-intro',
  templateUrl: 'quiz-intro.html',
})
export class QuizIntroPage {
  state: any = null;

  constructor(
    private alertCtrl: AlertController,
    private constants: ConstantsProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage
  ) {
  }

  ionViewWillEnter() {
    this.constants.setCurrentPage('quiz-intro');
    this.storage.get('state').then((value) => {
      if (value) {
        let quiz = value.quiz;
        let number = value.number;
        if (number >= quiz.length) {
          this.state = null;
        } else {
          this.state = value;
        }
      }
    });
  }

  onClickStart(state) {
    if (state != null) {
      this.storage.get('state').then((value) => {
        this.navCtrl.push(QuizPage, {state: value}, {animate: false});
      });
    } else {
      this.navCtrl.push(QuizPage, {state: state}, {animate: false});
    }
  }

  onClickRestart() {
    let confirm = this.alertCtrl.create({
      title: 'Restart',
      message: 'Are you sure you want to restart? Your previous session will be cleared.',
      buttons: [
        { text: 'Cancel' },
        { text: 'Restart', handler: () => { this.onClickStart(null); } }
      ]
    });
    confirm.present();
  }

}
